export  default function slidersInit(){
	
	(function($){
	
	
    //slider on front_page
    $(".owl-1").owlCarousel({
        responsive: {
         0: {
           items:1
         },
         640: {
           items:2
         }
       
        },
        items: 2,
        loop: true,
        margin: 10,
        autoplay: 2000,
        stopOnHover: true,
        autoHeight: true,
        smartSpeed: 1000,
        slideTransition: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
        nav: true,
        dots: false,
        // autoHeight: true
        navText:[`<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="74" height="74" viewBox="0 0 74 74">
        <defs>
          <filter id="Ellipse_6" x="0" y="0" width="74" height="74" filterUnits="userSpaceOnUse">
            <feOffset input="SourceAlpha"/>
            <feGaussianBlur stdDeviation="5" result="blur"/>
            <feFlood flood-color="#f6aea2"/>
            <feComposite operator="in" in2="blur"/>
            <feComposite in="SourceGraphic"/>
          </filter>
        </defs>
        <g id="Group_670" data-name="Group 670" transform="translate(-527 -2885)">
          <g transform="matrix(1, 0, 0, 1, 527, 2885)" filter="url(#Ellipse_6)">
            <circle id="Ellipse_6-2" data-name="Ellipse 6" cx="22" cy="22" r="22" transform="translate(15 15)" fill="#f6aea2"/>
          </g>
          <path id="Polygon_2" data-name="Polygon 2" d="M9,0l9,16H0Z" transform="translate(554 2931) rotate(-90)" fill="#fff"/>
        </g>
      </svg>
      `, `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="74" height="74" viewBox="0 0 74 74">
      <defs>
        <filter id="Ellipse_5" x="0" y="0" width="74" height="74" filterUnits="userSpaceOnUse">
          <feOffset input="SourceAlpha"/>
          <feGaussianBlur stdDeviation="5" result="blur"/>
          <feFlood flood-color="#f6aea2"/>
          <feComposite operator="in" in2="blur"/>
          <feComposite in="SourceGraphic"/>
        </filter>
      </defs>
      <g id="Group_671" data-name="Group 671" transform="translate(-765 -2885)">
        <g transform="matrix(1, 0, 0, 1, 765, 2885)" filter="url(#Ellipse_5)">
          <circle id="Ellipse_5-2" data-name="Ellipse 5" cx="22" cy="22" r="22" transform="translate(15 15)" fill="#f6aea2"/>
        </g>
        <path id="Polygon_1" data-name="Polygon 1" d="M9,0l9,16H0Z" transform="translate(812 2913) rotate(90)" fill="#fff"/>
      </g>
    </svg>`],
    });






    //slider partnerzy

    $(".owl-2").owlCarousel({
        responsive: {
            0: {
                items: 3,
                margin: 40,
                nav: false
               
            },
            640: {
                items: 4,
                margin: 40,
                nav: false
            },
            830: {
                items: 4,
                margin: 60,
                nav: true,
            },
            1200: {
                items: 4,
                margin: 50
            }
        },
        loop: true,

        autoplay: 2000,
        stopOnHover: true,
        smartSpeed: 850,
        slideTransition: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
        nav: true,
        // autoplayTimeout:1000,
        dots: false,
        autoHeight: true,
        	navText:[`<svg xmlns="http://www.w3.org/2000/svg" width="28" height="53" viewBox="0 0 28 53">
            <text id="_" data-name="&gt;" transform="translate(28 10) rotate(180)" fill="#b2b2b2" font-size="40" font-family="SegoeUI, Segoe UI"><tspan x="0" y="0">&gt;</tspan></text>
          </svg>`, `<svg xmlns="http://www.w3.org/2000/svg" width="28" height="53" viewBox="0 0 28 53">
          <text id="_" data-name="&gt;" transform="translate(0 43)" fill="#b2b2b2" font-size="40" font-family="SegoeUI, Segoe UI"><tspan x="0" y="0">&gt;</tspan></text>
        </svg>`],
    });
		
	})(jQuery);
	
}
 