import generalInit from './modules/general';
import slidersInit from './modules/sliders';
import menuInit from './modules/menu';
import galleryInit from './modules/wp-gallery';
import loadMoreInit from './modules/loadmore';
import matchHeighttoExport from './modules/match_height'



(function($){
	



	// $(document).foundation();
		
	$(document).ready(function(){

		generalInit();
		slidersInit();
		menuInit();
		galleryInit();
		loadMoreInit(); 
		matchHeighttoExport();

//animacja-oferty
		$(' .anim-plan .but').click(function (e) {
			e.preventDefault()
			let element_id = $(this).attr('data-show-panel');
			let target = ".anim-text-" + element_id;
			$(target).show();
			$(this).addClass("pink-underline");
			$(this).siblings().removeClass("pink-underline");
			$(target).siblings().hide();
		})
		
//gdy animujemy dynamiczny element
		$('body').on('click', '.p-more', function(){ 
			$('.p-more-text').slideToggle();
			$(this).toggleClass('p-more-color');
		});


///scroll

$(' .mouse-down img').click(function () {
	$('html,body').animate({
		scrollTop: $(this).offset().top + 130
	}, 500 );
	// }, 500, "easeOutCirc");
	//To use more animation, like this above, you need easing plugin. In this case is: jQuery Easing Plugin (version 1.3)
});


//animated-rect
$(' .animated-rect .rect').hover(function (e) {
	e.preventDefault()

	let element_id = $(this).attr('data-show-rect-text');
	let target = ".rec-text-" + element_id;
	$('.rect-text').hide();
	$(target).show();

	$(".arrow").show();
	let arrow = ".arrow-" + element_id;
	console.log(arrow);
	$(arrow).hide();


	// $().delay(1000).fadeOut(300); 


	
	$(this).siblings().removeClass();
	$(this).siblings().addClass("cell small-4 rect p-font-smaller");

	$(this).removeClass();
	$(this).addClass("cell small-8 rect p-font-smaller");



})
	
	

	});
	
})(jQuery);